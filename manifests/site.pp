Package {
	require => File['/etc/apt/apt.conf.d/local-recommends']
}

File {
	owner  => root,
	group  => root,
	mode   => '0444',
	ensure => file,
}

Exec {
	path => '/usr/bin:/usr/sbin:/bin:/sbin'
}

Service {
	hasrestart => true,
	hasstatus  => true,
}

node default {
	include site
	include munin
	include syslog_ng
	include sudo
	include ssh
	include debian_org
	include monit
	include time
	include ssl
	include hardware
	include nagios::client
	include resolv
	include roles
	include motd
	include unbound
	include bacula::client
	include autofs
	include dsa_lvm
	include grub
	include multipath
	include popcon
	include portforwarder
	include postgres
	include haveged
	include huge_mem
	include tcp_bbr

	if $::lsbdistcodename == squeeze {
		include roles::udldap::client
	} else {
		include roles::pubsub::client
		class { 'roles::udldap::client':
			ensure => absent
		}
	}

	if getfromhash($site::nodeinfo, 'ganeti') {
		include ganeti2
	}

	if $::kernel == 'Linux' {
		include linux
		include acpi
	}

	if $::mta == 'exim4' {
		if getfromhash($site::nodeinfo, 'heavy_exim') {
			include exim::mx
		} else {
			include exim
		}
	} elsif $::mta == 'postfix' {
		include postfix
	} else {
		include exim
	}

	if $::apache2 {
		include apache2
	}

	if $::hostname in [geo1,geo2,geo3] {
		include named::geodns
	}

	if $::hostname in [buxtehude,milanollo,lw01,lw02,lw03,lw04,lw09,lw10,senfter,gretchaninov,sibelius] {
		include nfs_server
	}

	if $::brokenhosts {
		include hosts
	}

	if $::samhain {
		include samhain
	}

	if $::hostname in [geo3,wieck] {
		include debian_org::radvd
	}

	if $::spamd {
		munin::check { 'spamassassin': }
	}

	if $::hostname == 'pkgmirror-csail' {
		include varnish_pkgmirror
	}
}
