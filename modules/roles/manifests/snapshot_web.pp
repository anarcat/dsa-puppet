class roles::snapshot_web {
	include apache2
	include apache2::rewrite

	# snapshot abusers
	#  61.69.254.110 - 20180705, mirroring with wget
	# 20180821 large amount of requests way too fast from some amazon AWS instances
	#  18.185.157.46
	#  18.194.174.202
	#  18.184.181.169
	#  18.184.5.230
	#  18.194.137.96
	#  18.197.147.183
	#  3.120.39.137
	#  3.120.41.69
	#  35.158.129.130
	#  52.59.199.25
	#  52.59.228.158
	#  52.59.245.42
	#  52.59.253.41
	#  52.59.71.13
	# 20180821 mirroring
	#  99.137.191.34
	# 20181110 crawler
	#  51.15.215.91
	# 20181222, excessive number of requests
	#  208.91.68.213
	# 198.11.128.0/18
	@ferm::rule { 'dsa-snapshot-abusers':
		prio  => "005",
		rule  => "saddr (61.69.254.110 18.128.0.0/9 3.120.0.0/14 35.156.0.0/14 52.58.0.0/15 99.137.191.34 51.15.215.91 208.91.68.213 198.11.128.0/18) DROP",
	}

	ensure_packages ( [
		"libapache2-mod-wsgi",
		], {
		ensure => 'installed',
	})

	apache2::site { '020-snapshot.debian.org':
		site   => 'snapshot.debian.org',
		content => template('roles/snapshot/snapshot.debian.org.conf.erb')
	}

	case $::hostname {
		'lw07': {
			$ipv4addr        = '185.17.185.185'
			$ipv6addr        = '2001:1af8:4020:b030:deb::185'
			$ipv6addr_apache = '2001:1af8:4020:b030:deb::187'
		}
		'sallinen': {
			$ipv4addr        = '193.62.202.27'
			$ipv6addr        = '2001:630:206:4000:1a1a:0:c13e:ca1b'
			$ipv6addr_apache = '2001:630:206:4000:1a1a:0:c13e:ca1a'
		}
		default: {
			fail ( "unknown host $::hostname for snapshot_web." )
		}
	}

	@ferm::rule { 'dsa-snapshot-connlimit':
		domain => '(ip ip6)',
		prio  => "005",
		rule  => "proto tcp mod state state (NEW) interface ! lo daddr (${ipv4addr} ${ipv6addr})  mod multiport destination-ports (80 443) mod connlimit connlimit-above 3 DROP;
		          proto tcp mod state state (NEW) interface ! lo                                                dport 6081                 mod connlimit connlimit-above 3 DROP
		           ",
	}

	# varnish cache
	###############
	@ferm::rule { 'dsa-nat-snapshot-varnish-v4':
		table => 'nat',
		chain => 'PREROUTING',
		rule  => "proto tcp daddr ${ipv4addr} dport 80 REDIRECT to-ports 6081",
	}

	varnish::config { 'default':
		listen  => [
			':6081',
			"[$ipv6addr]:80"
			],
		backend => 'file,/var/lib/varnish/varnish_storage.bin,8G',
		content => template('roles/snapshot/snapshot.debian.org.vcl.erb'),
	}

	# the ipv6 port 80 is owned by varnish
	file { '/etc/apache2/ports.conf':
		content  => @("EOF"),
			Listen 0.0.0.0:80
			Listen [$ipv6addr_apache]:80
			| EOF
		require => Package['apache2'],
		notify  => Service['apache2'],
	}

	# haproxy ssl termination
	#########################
	include haproxy
	file { '/etc/haproxy/haproxy.cfg':
		content => template('roles/snapshot/haproxy.cfg.erb'),
		require => Package['haproxy'],
		notify  => Service['haproxy'],
	}
	ssl::service { 'snapshot.debian.org':
		notify  => Service['haproxy'],
		key => true,
	}
}
