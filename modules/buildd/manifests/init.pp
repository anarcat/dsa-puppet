class buildd ($ensure=present) {
	# Do nothing until we get the buildd user from ldap
	if $::buildd_user_exists {
		#
		# home directory
		#
		file { '/home/buildd':
			ensure  => directory,
			mode    => '2755',
			group   => buildd,
			owner   => buildd,
		}


		#
		# sbuild configuration, including chroots
		#
		include schroot

		package { 'sbuild':
			ensure => installed,
			tag    => extra_repo,
		}
		package { 'libsbuild-perl':
			ensure => installed,
			tag    => extra_repo,
			before => Package['sbuild']
		}
		file { '/etc/sbuild/sbuild.conf':
			source  => 'puppet:///modules/buildd/sbuild.conf',
			require => Package['sbuild'],
		}
		if $has_srv_buildd {
			concat::fragment { 'dsa-puppet-stuff--buildd-update-schroots':
				target => '/etc/cron.d/dsa-puppet-stuff',
				content  => @(EOF)
					13 22 * * 0,3 root PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin setup-all-dchroots buildd
					| EOF
			}
		}
		exec { 'add-buildd-user-to-sbuild':
			command => 'adduser buildd sbuild',
			onlyif  => "getent group sbuild > /dev/null && ! getent group sbuild | grep '\\<buildd\\>' > /dev/null"
		}


		#
		# dupload configuration
		#
		package { 'dupload':
			ensure => installed,
		}
		file { '/etc/dupload.conf':
			source  => 'puppet:///modules/buildd/dupload.conf',
			require => Package['dupload'],
		}
		include ferm::ftp_conntrack


		#
		# Aptitude killer
		#
		package { 'python-psutil':
			ensure => installed,
		}
		file { '/usr/local/sbin/buildd-schroot-aptitude-kill':
			source  => 'puppet:///modules/buildd/buildd-schroot-aptitude-kill',
			mode    => '0555',
		}

		concat::fragment { 'dsa-puppet-stuff--buildd-aptitude-killer':
			target => '/etc/cron.d/dsa-puppet-stuff',
			content  => @(EOF)
				*/5 * * * * root /usr/local/sbin/buildd-schroot-aptitude-kill
				| EOF
		}


		#
		# GPG/SSH key generation
		#
		file { '/home/buildd/.gnupg':
			ensure  => directory,
			mode    => '700',
			group   => buildd,
			owner   => buildd,
		}
		file { '/home/buildd/.gnupg/gpg.conf':
			content  => "personal-digest-preferences SHA512\n",
			group   => buildd,
			owner   => buildd,
		}

		if ! $::buildd_key {
			exec { 'create-buildd-key':
				command => '/bin/su - buildd -c \'mkdir -p -m 02700 .ssh && ssh-keygen -C "`whoami`@`hostname` (`date +%Y-%m-%d`)" -P "" -f .ssh/id_rsa -q\'',
				onlyif  => '/usr/bin/getent passwd buildd > /dev/null && ! [ -e /home/buildd/.ssh/id_rsa ]'
			}
		}

		#
		# buildd/pybuildd configuration
		#
		if $::hostname in [x86-bm-01,x86-grnet-01,zani] {
			#
			# pybuildd configuration
			#
			package { 'buildd':
				ensure => purged,
			}

			package { ['python3-retrying', 'python3-yaml']:
				ensure => installed,
			}
			file { '/home/buildd/.profile':
				content  => @(EOT),
					export XDG_RUNTIME_DIR="/run/user/$(id -u)"
					export DBUS_SESSION_BUS_ADDRESS="unix:path=${XDG_RUNTIME_DIR}/bus"
					| EOT
				group   => buildd,
				owner   => buildd,
			}
			file { '/home/buildd/logs':
				ensure  => directory,
				mode    => '2750',
				group   => buildd,
				owner   => buildd,
			}
			file { '/var/lib/systemd/linger':
				ensure  => directory,
				mode    => '755',
			}
			file { "/var/lib/systemd/linger/buildd":
				ensure => present,
			}
			file { '/etc/systemd/journald.conf.d':
				ensure  => directory,
				mode    => '755',
			}
			file { '/etc/systemd/journald.conf.d/persistency.conf':
				source => 'puppet:///modules/dsa_systemd/persistency.conf',
			}

			# Make sure that the build directory have the correct permissions.
			# This should go away once pybuildd issue #3 is solved.
			file { '/home/buildd/build':
				ensure  => directory,
				mode    => '2750',
				group   => buildd,
				owner   => buildd,
			}
			# work around https://salsa.debian.org/wb-team/pybuildd/issues/11
			concat::fragment { 'dsa-puppet-stuff--pybuildd-expire-logs':
				target => '/etc/cron.d/dsa-puppet-stuff',
				content  => @(EOF)
					@daily buildd [ -d ~buildd/logs ] && find ~buildd/logs -type f -mtime +90 -delete
					| EOF
			}
		} else {
			#
			# buildd configuration
			#
			file { '/home/buildd/build':
				ensure  => directory,
				mode    => '2750',
				group   => buildd,
				owner   => buildd,
			}
			file { '/home/buildd/logs':
				ensure  => directory,
				mode    => '2750',
				group   => buildd,
				owner   => buildd,
			}
			file { '/home/buildd/old-logs':
				ensure  => directory,
				mode    => '2750',
				group   => buildd,
				owner   => buildd,
			}
			file { '/home/buildd/upload-security':
				ensure  => directory,
				mode    => '2750',
				group   => buildd,
				owner   => buildd,
			}
			file { '/home/buildd/stats':
				ensure  => directory,
				mode    => '2755',
				group   => buildd,
				owner   => buildd,
			}
			file { '/home/buildd/stats/graphs':
				ensure  => directory,
				mode    => '2755',
				group   => buildd,
				owner   => buildd,
			}
			file { '/home/buildd/upload':
				ensure  => directory,
				mode    => '2755',
				group   => buildd,
				owner   => buildd,
			}
			file { '/home/buildd/.forward':
				content  => "|/usr/bin/buildd-mail\n",
				group   => buildd,
				owner   => buildd,
			}

			package { 'buildd':
				ensure => installed,
			}
			file { '/etc/buildd/buildd.conf':
				source  => 'puppet:///modules/buildd/buildd.conf',
				require => Package['buildd'],
			}

			if (versioncmp($::lsbmajdistrelease, '9') >= 0) {
				site::aptrepo { 'buildd.debian.org':
					ensure => absent,
				}
				file { '/etc/apt/apt.conf.d/puppet-https-buildd':
					ensure => absent,
				}
			} else {
				package { 'apt-transport-https':
					ensure => installed,
				}
				site::aptrepo { 'buildd.debian.org':
					key        => 'puppet:///modules/buildd/buildd.debian.org.gpg',
					url        => 'https://apt.buildd.debian.org/',
					suite      => 'jessie',
					components => 'main',
					require    => Package['apt-transport-https'],
				}
				file { '/etc/apt/apt.conf.d/puppet-https-buildd':
					content => "Acquire::https::apt.buildd.debian.org::CaInfo \"/etc/ssl/ca-debian/ca-certificates.crt\";\n",
				}
			}

			concat::fragment { 'dsa-puppet-stuff--buildd':
				target => '/etc/cron.d/dsa-puppet-stuff',
				source  => 'puppet:///modules/buildd/cron.d-dsa-buildd',
				require => Package['debian.org']
			}
			service { 'buildd':
				enable => false,
				ensure => 'stopped',
			}
		}
	}
}
