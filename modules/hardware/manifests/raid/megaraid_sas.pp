class hardware::raid::megaraid_sas {
	if ($::megaraid_sas and $::debarchitecture == "amd64") {
		include debian_org::apt_restricted

		package { 'megacli':
			ensure  => installed,
		}

		file { '/usr/local/sbin/megacli':
			ensure => 'link',
			target => '/opt/MegaRAID/MegaCli/MegaCli64',
		}
	} else {
		package { 'megacli':
			ensure  => purged,
		}
		file { '/usr/local/sbin/megacli':
			ensure => 'absent',
		}
	}
	site::aptrepo { 'debian.restricted.megaraid_sas':
		ensure => absent,
	}
}
