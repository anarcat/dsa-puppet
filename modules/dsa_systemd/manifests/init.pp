class dsa_systemd {
	file { '/etc/systemd/journald.conf':
		source => 'puppet:///modules/dsa_systemd/journald.conf',
	}

	dsa_systemd::mask { 'sys-kernel-debug-tracing.mount': }
	dsa_systemd::mask { 'sys-kernel-debug.mount': }

	include stretch::network_online

	if $::hostname == 'godard' {
		exec {'mkdir -p /etc/systemd/journald.conf.d':
			unless => 'test -d /etc/systemd/journald.conf.d',
		}
		file { '/etc/systemd/journald.conf.d/persistency.conf':
			source => 'puppet:///modules/dsa_systemd/persistency.conf',
		}
	}

	file { '/usr/local/sbin/systemd-cleanup-failed':
		source => 'puppet:///modules/dsa_systemd/systemd-cleanup-failed',
		mode   => '0555',
	}
	concat::fragment { 'dsa-puppet-stuff--systemd-cleanup-failed':
		target => '/etc/cron.d/dsa-puppet-stuff',
		content  => @("EOF"),
				*/10 * * * * root /usr/local/sbin/systemd-cleanup-failed
				| EOF
	}
}
